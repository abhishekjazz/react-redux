const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const produrl = 'https://api.submittaltree.com:5001/demo/api/v2/';

module.exports = merge(common, {
  mode: 'production',
  externals: {
    // global app config object
    config: JSON.stringify({
        apiUrl: produrl
    })
}
});