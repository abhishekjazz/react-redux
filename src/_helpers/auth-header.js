export function authHeader() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('jwt'));

    if (user) {
        return { 'accessToken':user, 'Authorization': 'Basic YWRtaW46UTFXMkUzUjRUNQ=='   };
    } else {
        return {};
    }
}