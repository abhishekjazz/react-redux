export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
        text: 'NEW',
      },
    },
    {
      name: 'User',
      url: '/user',
      icon: 'icon-speedometer'
    },
    {
      name: 'Plan',
      url: '/plans',
      icon: 'icon-cursor',
      children: [
        {
          name: 'PRO',
          url: '/plan/pro',
          icon: 'icon-cursor',
        },
        {
          name: 'Premium',
          url: '/plan/pre',
          icon: 'icon-cursor',
        }
      ],
    },
  ],
};
