import React from 'react';

const Home = React.lazy(() => import('./views/HomePage'));
const List = React.lazy(() => import('./views/Users'));

const routes = [
  { path: '/', exact: true, name: 'Home', component : Home },
  { path: '/dashboard', name: 'Dashoard', component : Home },
  { path: '/user',   exact: true, name: 'User List', component : List }
];

export default routes;
