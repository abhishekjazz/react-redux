export default (nextState, replace) => {
  const authenticated = localStorage.getItem('user');
  console.log(authenticated);

  if (!authenticated && nextState) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname }
    })
  }
}
