// import "isomorphic-fetch";
import config from 'config'; 

import { authHeader } from '../_helpers';
import { APIURL } from "../_constants/apiStore";

url = `${config.url}${APIURL}`

export async function post(url, payload) {
  // TODO change to be Authorization Bearer (needs a backend change as well)
  return await fetch(`${url}/${url}`, {
    method: "POST",
    body: JSON.stringify(payload),
    json: true,
    headers: authHeader
  });
}

export async function get(url) {
  console.log("=======",`${url}/${url}`);
  return await fetch(`${url}/${url}`, {
    json: true,
    headers: authHeader
  });
}

export async function patch(url, payload) {
  return await fetch(`${url}/${url}`, {
    method: "PATCH",
    body: JSON.stringify(payload),
    json: true,
    headers: authHeader
  });
}

export async function put(url, payload) {
  return await fetch(`${url}/${url}`, {
    method: "PUT",
    body: JSON.stringify(payload),
    json: true,
    headers: authHeader
  });
}
export async function putImage(url, payload) {
  return await fetch(`${url}/${url}`, {
    method: "POST",
    body: JSON.stringify(payload),
    headers: {
      snaphuntjwttoken: localStorage.getItem("snaphuntJwtToken"),
      "content-type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("Authorization")}`,
      "cache-control": "no-cache",
      "Content-Length": JSON.stringify(payload).length,
      keepAlive: true
    }
  });
}
