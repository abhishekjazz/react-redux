import { userConstants } from '../_constants';

export function registration(state = {}, action) {
  switch (action.type) {
    case userConstants.REGISTER_REQUEST:
      console.log('inside 1');
      return { registering: true };
    case userConstants.REGISTER_SUCCESS:
      console.log('inside 2 ');

      return {};
    case userConstants.REGISTER_FAILURE:
      console.log('inside 3');

      return {};
    default:
      return state
  }
}
