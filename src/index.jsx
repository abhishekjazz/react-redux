import 'react-app-polyfill/ie9'; // For IE 9-11 support
import 'react-app-polyfill/stable';
// import 'react-app-polyfill/ie11'; // For IE 11 support
import './polyfill'
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import { store } from './_helpers';
import { App } from './App';
// setup fake backend
// import { configureFakeBackend } from './_helpers';
// configureFakeBackend();
// provider is use to make store available to all components.
render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);