import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../../_actions';

class Footer extends Component {
       render() {
        const { user, users } = this.props;        
        return (
            <div className="col-md-6 col-md-offset-3">
                <h1>Footer</h1>
            </div>
        );
    }
}
 
function mapStateToProps(state) {
    // console.log(state)
    
    return {};
}

const connectedFooter = connect(mapStateToProps)(Footer);
export { connectedFooter as Footer };
