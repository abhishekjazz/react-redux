import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../../_actions';

class Aboutus extends React.Component {

    render() {
        console.log('state')
        return (
            <div className="col-md-6 col-md-offset-3">
                <h1>Hi</h1>
                <p>You're logged in with React!!</p>
                <h3>Aboutus</h3>
            </div>
        );
    }
} 

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedAboutus = connect(mapStateToProps)(Aboutus);
export { connectedAboutus as Aboutus };
