import { HomePage } from './HomePage';
import { LoginPage } from './LoginPage';
import { RegisterPage } from './RegisterPage';
import { Header } from './Header';
import { Footer } from './Footer';
import { Aboutus } from './Aboutus';
import { Users } from './Users';



import { Login, Page404, Page500, Register } from './Pages';

export {
  HomePage, LoginPage, RegisterPage, Header, Footer,Aboutus, Users,
  Page404,
  Page500,
};

