import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../../_actions';
import { Header } from '../Header';
import { Footer } from '../Footer';
// import Button from '@material/react-button';

class HomePage extends Component {
    render() {    
        return (
            <div className="col-md-6 col-md-offset-3">
               <h1>DASHBOARD</h1>
            </div>
        );
    }
}
 
function mapStateToProps(state) {
    // console.log(state)
    const { users, authentication } = state;
    const { user } = authentication;
    
    return {
        user,
        users
    };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage };
