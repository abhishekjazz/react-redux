import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../../_actions';

class Header extends Component {
    componentDidMount() {
        this.props.dispatch(userActions.getAll());
    }

    handleDeleteUser(id) {
        return (e) => this.props.dispatch(userActions.delete(id));
    }
    logout() {
        return (e) => this.props.dispatch(userActions.logout());
    }

    render() {
        const { user, users } = this.props;        
        return (
            <div className="col-md-6 col-md-offset-3">
                <h1>Header</h1>
               
            </div>
        );
    }
}
 
function mapStateToProps(state) {
    // console.log(state)    
    return {};
}

const connectedHeader = connect(mapStateToProps)(Header);
export { connectedHeader as Header };
