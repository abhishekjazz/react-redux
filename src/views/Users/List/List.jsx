import React, { Component } from 'react';
import { connect } from 'react-redux';

import { userActions } from '../../../_actions';
import { Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import DataTable from 'react-data-table-component';
import styled from 'styled-components';

const columns = [{
  name: 'S.no.',
  selector: '_id',
  sortable: true
},{
  name: 'Name',
  selector: 'name',
  sortable: true,
},
{
  name: 'Email',
  selector: 'email',
  sortable: true,
  right: false,
},
{
  name: 'address',
  selector: 'address',
  sortable: true,
  right: false,
},
{
  name: 'phone',
  selector: 'phone',
  sortable: true,
  right: false,
}]

const TextField = styled.input`
  height: 32px;
  width: 300px;
  border-radius: 3px;
  border: 1px solid #e5e5e5;
  padding: 16px;

  &:hover {
    cursor: pointer;
  }
`;

const Filter = ({ onFilter }) => (
  <TextField
    id="search"
    type="search"
    role="search"
    placeholder="Search Title"
    onChange={e => onFilter(e)}
  />
);

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: false,
      totalRows: 0,
      perPage: 25,
      page : 1,
      search:''
    };
  }
  componentDidMount() {
    this.setState({ loading: true });
    this.props.dispatch(userActions.getAll({ page: this.state.page, perPage: this.state.perPage, search : this.state.search }));    
  }

  static getDerivedStateFromProps(nextProps, prevState) {   
    if (nextProps.users.items) {
      return {
        data: nextProps.users.items.userlist,
        totalRows: nextProps.users.items.total,
        loading: false
      };
    } else
      return null
    }
  handleDeleteUser(id) {
    return (e) => this.props.dispatch(userActions.delete(id));
  }
  logout() {
    return (e) => this.props.dispatch(userActions.logout());
  }

  handlePageChange = page => {
    const { perPage, search } = this.state;    
    this.setState({ loading: true, page });
    this.props.dispatch(userActions.getAll({ page: page, perPage: perPage, search:search }));
  };

  handlePerRowsChange = (perPage, page) => {
    const { search } = this.state;

    this.props.dispatch(userActions.getAll({ page: page, perPage: perPage,  search:search }));
    this.setState({ loading: true,search });
  };

  handleChange = (eve) => {
    eve.preventDefault();
    console.log(eve);
    this.setState({ loading: true,search:eve.target.value });
    const { perPage, page, search } = this.state;
    this.props.dispatch(userActions.getAll({ page: page, perPage: perPage, search: search  }));
  }

  render() {
    const { data, loading, totalRows, perPage, page } = this.state;
    const {users} = this.props;
    const paginationRowsPerPageOptions = [25,50,100,500];
    return (
      <div className="animated fadeIn">
        {users.error && <span className="text-danger">ERROR: {users.error}</span>}
        {
          users.items &&
          <DataTable
            title="User List"
            columns={columns}
            data={data}
            progressPending={loading}
            pagination
            paginationServer
            subHeader
            subHeaderComponent={<Filter onFilter={this.handleChange} />}
            paginationRowsPerPageOptions = {paginationRowsPerPageOptions}
            paginationTotalRows={totalRows}
            paginationPerPage = {perPage}
            paginationDefaultPage = {page}
            onChangeRowsPerPage={this.handlePerRowsChange}
            onChangePage={this.handlePageChange}
          />
        }
        {/* <Row>
              <Col>
                <Card>
                  <CardHeader>
                    <strong><i className="icon-info pr-1"></i>User List</strong>
                  </CardHeader>
                  <CardBody>
                      <Table responsive striped hover>
                      <thead>
                  <tr>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Addree</th>
                    <th>Phone</th>
                  </tr>
                  </thead>
                        <tbody>
                        {
                            users.items && users.items.userlist.map((value) => {
                              return (
                                <tr key={value._id}>
                                  <td>{value.name}</td>
                                  <td><strong>{value.email}</strong></td>
                                  <td>{value.address}</td>
                                  <td>{value.phone}</td>
                                </tr>
                              )
                            })
                          }
                        </tbody>
                      </Table>
                  </CardBody>
                </Card>
              </Col>
            </Row> */}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { users, authentication } = state;
  const { user } = authentication;
  return {
    user,
    users
  };
}

const connectedList = connect(mapStateToProps)(List);
export { connectedList as List };
