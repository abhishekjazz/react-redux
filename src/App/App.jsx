import React, { Component, Suspense, lazy } from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import { connect } from 'react-redux';

import { history } from '../_helpers';
import { alertActions } from '../_actions';
import { PrivateRoute } from '../_components';
import { PublicRoute } from '../_components';

import { LoginPage } from '../views/LoginPage';
import { RegisterPage } from '../views/RegisterPage';
import '../App.scss';

const Aboutus = lazy(() => import('../views/Aboutus')); 
const DefaultLayout = lazy(() => import('../_containers/DefaultLayout'));

const Page404 = lazy(() => import('../views/Pages/Page404'));
const Page500 = lazy(() => import('../views/Pages/Page500'));

// import { DefaultLayout } from "../_containers/DefaultLayout";
const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;
class App extends Component {
    constructor(props) {
        super(props);

        const { dispatch } = this.props;
        history.listen((location, action) => {
            // clear alert on location change
            dispatch(alertActions.clear());
        });
    }

    render() {
        const { alert } = this.props;
        return (
            <Router history={history}>
                {/* <div> */}
                    <Suspense fallback={loading()}>
                        <Switch>
                        <Route exact path="/404" name="Page 404"render={props => <Page404 {...props} />} />
                        <Route exact path="/500" name="Page 500" render={props => <Page500 {...props} />} />
                        <Route exact path="/about" name="Page 500" render={props => <Aboutus {...props} />} />
                        <PublicRoute exact path="/login" component={LoginPage} />
                        <PublicRoute exact path="/register" component={RegisterPage} />
                        <PrivateRoute  path="/" component={DefaultLayout} />
                        </Switch>
                    </Suspense>
                {/* </div> */}
            </Router>
        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 