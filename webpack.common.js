const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;


module.exports = {

    // return {
    entry: "./src/index.jsx",
    resolve: {
        extensions: ['.js', '.jsx']
    },
    watchOptions: {
        ignored:'/node_modules/'
    },
    module: {
        rules: [
            {

                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            { // config for sass compilation
                test: /\.scss$/,
                use: [
                    // fallback to style-loader in development
                    process.env.NODE_ENV !== 'production' ? 'style-loader' : MiniCssExtractPlugin.loader,
                    "css-loader",
                    "sass-loader"
                ]
            },
            { test: /(\.css$)/, loaders: ['style-loader', 'css-loader'] },
            { test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/, loader: 'url-loader?limit=100000' }
        ]
    },
    output: {
        path: __dirname + '/dist',
        publicPath: '/',
        filename: 'bundle.js'
    },
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                parallel: true, //Use multi-process parallel running to improve the build speed. Default number of concurrent runs: os.cpus().length - 1.
                                //Parallelization can speedup your build significantly and is therefore highly recommended.
                uglifyOptions: {
                    output: {
                        comments: /@license/i, // Extract all legal comments (i.e. /^\**!|@preserve|@license|@cc_on/i) and preserve /@license/i comments.
                    },
                },
                extractComments: true,
            }),
        ],
    },
    plugins: [new HtmlWebpackPlugin({
        template: './src/index.html',
        filename: "./index.html"
    }),
    new MiniCssExtractPlugin({ // plugin for controlling how compiled css will be outputted and named
        filename: "[name].css",
        chunkFilename: "[id].css"
    }),
    new webpack.HotModuleReplacementPlugin(),
    new CleanWebpackPlugin({
        // Simulate the removal of files
        //
        // default: false
        dry: true,

        // Write Logs to Console
        // (Always enabled when dry is true)
        //
        // default: false
        verbose: true,
        cleanOnceBeforeBuildPatterns: ["css/*.*", "js/*.*", "fonts/*.*", "images/*.*"]
    }),
    new BundleAnalyzerPlugin({
        analyzerMode: 'disaabled',
        generateStatsFile :true,
        statsOptions: { source: false }
    })
    ],
    devServer: {
        hot:true,
        historyApiFallback: true
    }
    // }
}