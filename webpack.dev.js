const merge = require('webpack-merge');
const common = require('./webpack.common.js');

const devurl = 'https://devapi.submittaltree.com:5001/demo/api/v2/';
   
module.exports = merge(common, {    
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist'
  },
  externals: {
    // global app config object
    config: JSON.stringify({
        apiUrl: devurl
    })
}
 });