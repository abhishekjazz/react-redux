var HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const Dotenv = require('dotenv-webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// var dotenv = Dotenv.config({path: __dirname + '/.env'});
const stagurl = 'https://api.submittaltree.com:5001/demo/api/v2/';
const devurl  = 'https://devapi.submittaltree.com:5001/demo/api/v2/';
const produrl = 'https://api.submittaltree.com:5001/demo/api/v2/';


module.exports = (env) => {
    return {
        mode: 'development',
        entry: "./src/index.jsx",
        resolve: {
            extensions: ['.js', '.jsx']
        },
        module: {
            rules: [
                {

                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                { // config for sass compilation
                    test: /\.scss$/,
                    use: [
                        // fallback to style-loader in development
                        process.env.NODE_ENV !== 'production' ? 'style-loader' : MiniCssExtractPlugin.loader,
                        "css-loader",
                        "sass-loader"
                    ]
                },
                { test: /(\.css$)/, loaders: ['style-loader', 'css-loader'] }, 
                { test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/, loader: 'url-loader?limit=100000' }
                // {
                //     test: /\.css$/,
                //     use: [
                //         "style-loader",
                //         {
                //             loader: "css-loader",
                //             options: {
                //                 modules: true
                //             }
                //         }
                //     ]
                // },
                // { // config for images
                //     test: /\.(png|svg|jpg|jpeg|gif)$/,
                //     use: [
                //         {
                //             loader: 'file-loader',
                //             options: {
                //                 outputPath: 'images',
                //             }
                //         }
                //     ],
                // },
                // { // config for fonts
                //     test: /\.(woff|woff2|eot|ttf|otf)$/,
                //     use: [
                //         {
                //             loader: 'file-loader',
                //             options: {
                //                 outputPath: 'fonts',
                //             }
                //         }
                //     ],
                // }
            ]
        },
        output: {
            path: __dirname + '/dist',
            publicPath: '/',
            filename: 'bundle.js'
        },
        plugins: [new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: "./index.html"
        }),
        new MiniCssExtractPlugin({ // plugin for controlling how compiled css will be outputted and named
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        new webpack.HotModuleReplacementPlugin(),
        // new webpack.DefinePlugin({
        //     "process.env.NODE_ENV": `./.env.${env=='dev'?'development':env=='prod'?'production':'staging'}`
        // }),
        new Dotenv({
            path: `./.env.${env.env == 'dev' ? 'development' : (env.env == 'prod' ? 'production' : 'staging')}`
        })
        ],
        devServer: {
            historyApiFallback: true
        },
        externals: {
            // global app config object
            config: JSON.stringify({
                apiUrl: env.env == 'dev' ? devurl : (env.env == 'prod' ? produrl : stagurl)

            })
        }
    }
}